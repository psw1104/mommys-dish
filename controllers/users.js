/**
 * Created by Woong on 2015. 7. 22..
 */
/**
 * Created by Woong on 2015. 7. 22..
 */
var mongoose = require('mongoose'),
    UsersModel = mongoose.model('UsersModel'),
    SessionModel = mongoose.model('SessionsModel'),
    hat = require('hat');

ObjectId = mongoose.Types.ObjectId;

/**
 * @api {post} /api/users 회원 가입(완료)
 * @apiName Create User
 * @apiGroup Users
 * @apiParam {String} user_key 기본사용자는 이메일,페북은 키값
 * @apiParam {String} email 이메일
 * @apiParam {String} password 해쉬로변환된 비번
 * @apiParam {String} type F,G,N facebook,google,normal
 */
/**
 * @api {get} /api/users/:id 회원 정보 요청
 * @apiName Get User
 * @apiGroup Users
 *
 */
/**
 * @api {put} /api/users/:id (앱)회원 정보 수정
 * @apiName Update User
 * @apiGroup Users
 *
 * @apiParam {String} email 음식이름
 * @apiParam {String} password 패스워드
 * @apiParam {String} name 이름
 * @apiParam {String} phone 전화번호
 * @apiParam {String} adress 주소
 */
/**
 * @api {put} /api/users/:id (앱)회원 정보 삭제
 * @apiName delete User
 * @apiGroup Users
 *
 */


exports.createUser = function(req, res, next) {

    console.log("CREATE USER");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //validate 부분은 나중에 추가
    //user의 아이디가 이미 있는지 확인,
    //있으면 이미 있다고 판단
    var doc = new UsersModel();
    doc.email = req.body.email;
    doc.type = req.body.type;
    if(req.body.type === 'N') {
        doc.password = req.body.password;
        doc.user_key = req.body.email;
    }else if (req.body.type === 'F') {
        doc.user_key = req.body.user_key;
        doc.password = "facebook";
    }else if (req.body.type === 'G') {
        doc.user_key = req.body.user_key;
        doc.password = "google";
    }else {//타입 에러

    }
    doc.name = "";
    doc.phone = "";
    doc.address = "";
    doc.birthdate = "";
    doc.residential = "";

    //이메일이 있는지 확인
    UsersModel.findOne({email : req.body.email,type: 'N'}, function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log("check exist email >>" + JSON.stringify(data));
            if(data) {                  //이메일이 없다면 저장
                res.status(201);
                res.json({code:201, message:"email exist already"});
            } else {
                doc.save(function(err, data) {
                    if(err) {
                        res.status(500);
                        res.json({code:500, message:err});
                    } else {
                        console.log(">>" + JSON.stringify(data));
                        res.status(200);
                       res.json({code:200, data:data});
                    }
                });
            }
        }
    });
};
exports.updateUser = function(req, res, next) {
    console.log("UPDATE USERS");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //벨리데이트 안쓸꺼면 이방식
    if(req.body.name != null) {
        UsersModel.findByIdAndUpdate(new ObjectId(req.params.id.toString()), {
            name: req.body.name,
            phone: req.body.phone,
            address: req.body.address,
            birthdate: req.body.birthdate,
            residential: req.body.residential
        }, function (err, data) {
            if (err) {
                res.status(500);
                res.json({code: 500, message: err});
            } else {
                console.log(">>" + JSON.stringify(data));
                res.status(200);
                res.json({code: 200, data: data});
            }
        });
    } else if (req.body.password != null) {
        UsersModel.findByIdAndUpdate(new ObjectId(req.params.id.toString()), {
            password: req.body.password
        }, function (err, data) {
            if (err) {
                res.status(500);
                res.json({code: 500, message: err});
            } else {
                console.log(">>" + JSON.stringify(data));
                res.status(200);
                res.json({code: 200, data: data});
            }
        });
    } else {
        console.log("err");
    }
};
exports.updateUserOrderdata = function(req, res, next) {
    console.log("UPDATE USERS");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //벨리데이트 안쓸꺼면 이방식
    UsersModel.findByIdAndUpdate(new ObjectId(req.params.id.toString()), {
        name: req.body.name,
        phone: req.body.phone,
        address: req.body.address
    }, function (err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            //console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};


exports.login = function(req, res, next) {

    console.log("LOGIN");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //validate 부분은 나중에 추가
    //var doc = new UsersModel();

    //doc.email = req.body.email;
    //doc.password = req.body.password;


    UsersModel.findOne({email : req.body.email , password:req.body.password}, function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {                //맞는 이메일과 비빌먼호가 있을때. 자동로그인을 하면 세션체크부터 하고 안되면 로그인으로
            if(data) {//맞는 이메일과 비밀번호가 있을때
                SessionModel.findOne({user_id : data._id.toString()},function(err,session_data) {
                    if(err) {
                        res.status(500);
                        res.json({code:500, message:err});
                    } else {
                        if(session_data) {// 이미 있다
                            res.status(200);
                            res.json({code:200, data:data, token:session_data.token});
                        }else {//없으니까 토큰 생성
                            var doc = new SessionModel();
                            doc.token = hat();
                            doc.user_id = data._id.toString();
                            doc.save(function(err, new_session_data) {
                                if (err) {
                                    res.status(500);
                                    res.json({code: 500, message: err});
                                } else {
                                    console.log(">>" + JSON.stringify(new_session_data));
                                    res.status(200);
                                    res.json({code: 200,data:data, token: new_session_data.token});
                                    return;
                                }
                            });
                        }
                    }
                });
            } else { //검색 하고 없을때
                res.status(404);
                res.json({code:404, message:"맞는 아이디가 없습니다."});
            }
        }
    });
};

//access token이 유효한지를 검사하는 함수
exports.checkSession = function(req, res, next) {
    console.log("CALL CHECK SESSION");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));
    SessionModel.findOne({token : req.body.token},function(err,data) {
        if(err) {
            res.status(500);
            console.log("FIND ERROR");

            res.json({code:500, message:err});
        } else {
            if(data) {// 세션 데이터 있음
                console.log("FIND SEESION");
                req.user_id = data.user_id;
                next();
            }else {//없으니까 토큰 생성
                //로그인 하시오!!!!
                console.log("NOT TOKEN");
                res.status(404);
                res.json({code:404, message:"로그인해주세요."});
            }
        }
    });
};
