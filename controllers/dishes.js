/**
 * Created by Woong on 2015. 7. 22..
 */
var mongoose = require('mongoose'),
    DishesModel = mongoose.model('DishesModel');

ObjectId = mongoose.Types.ObjectId;
//벨리데이트 빼고 완성
/**
 * @api {post} /api/dishes (운영)Dish 생성(완료)
 * @apiName Create Dish
 * @apiGroup Dishes
 * @apiDescription 운영
 * @apiParam {String} name_nav 음식이름(본토어)
 * @apiParam {String} name_eng 음식이름(영어)
 * @apiParam {String} price 가격
 * @apiParam {String} class_nav 분류(본토어)
 * @apiParam {String} class_eng 분류(영어)
 * @apiParam {String} ingredient_nav 재료(본토어)
 * @apiParam {String} ingredient_eng 재료(영어)
 * @apiParam {String} desc_nav 음식 설명(본토)
 * @apiParam {String} desc_eng 음식 설명(영어)
 * @apiParam {String} mommys_id 만든 마미스 id
 *
 * @apiSuccess {String} firstname Firstname of the User.
 *
 */
/**
 * @api {put} /api/dishes/:id (운영)Dish 수정(완료)
 * @apiName update Dish
 * @apiGroup Dishes
 *
 * @apiParam {String} name_nav 음식이름(본토어)
 * @apiParam {String} name_eng 음식이름(영어)
 * @apiParam {String} price 가격
 * @apiParam {String} class_nav 분류(본토어)
 * @apiParam {String} class_eng 분류(영어)
 * @apiParam {String} ingredient_nav 재료(본토어)
 * @apiParam {String} ingredient_eng 재료(영어)
 * @apiParam {String} desc_nav 음식 설명(본토)
 * @apiParam {String} desc_eng 음식 설명(영어)
 * @apiParam {String} mommys_id 만든 마미스 id
 */
/**
 *
 * @api {delete} /api/dishes/:id (운영)dish 삭제(완료)
 * @apiName Delete Dish
 * @apiGroup Dishes
 *
 */
/**
 * @api {get} /api/dishes (앱)dish 전체 리스트 요청(완료)
 * @apiName Get total Dish List
 * @apiGroup Dishes
 */
/**
 * @api {get} /api/dishes/:id (앱)dish 정보 요청(완료)
 * @apiName Get Dish
 * @apiGroup Dishes
 */
/**
 * @api {get} /api/dishes/:country (앱)해당 나라 음식 리스트 요청(완료)
 * @apiName Get Dishes in Country
 * @apiGroup Dishes
 */
/**
 * @api {get} /api/dishes/:mommys_id (앱)해당 마미스 음식 리스트 요청(미완료)
 * @apiName Get Dishes of mommys
 * @apiGroup Dishes
 */
exports.createDishes = function(req, res, next) {

    console.log("CREATE DISH");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //validate 부분은 나중에 추가
    var doc = new DishesModel;
    doc.name_nav = req.body.name_nav;
    doc.name_eng = req.body.name_eng;
    doc.price = req.body.price;
    doc.class = req.body.class;
    doc.ingredient_nav = req.body.ingredient_nav;
    doc.ingredient_eng = req.body.ingredient_eng;
    doc.class_nav = req.body.class_nav;
    doc.class_eng = req.body.class_eng;
    doc.desc_nav = req.body.desc_nav;
    doc.desc_eng = req.body.desc_eng;
    doc.country = req.body.country;
    doc.mommys_id = req.body.mommys_id;
    doc.avg_score = 0;

    doc.save(function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};
exports.getDishesList = function(req, res, next) {
    console.log("GET total DISH LIST");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    DishesModel.find(true, function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};
exports.getDishes = function(req, res, next) {
    console.log("GET DISH");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    DishesModel.findOne({_id:new ObjectId(req.params.dishes_id.toString())}, function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};
exports.updateDishes = function(req, res, next) {
    console.log("UPDATE DISH");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //벨리데이트 안쓸꺼면 이방식
    DishesModel.findByIdAndUpdate(new ObjectId(req.params.id.toString()), { $set: {
        name_nav: req.body.name_nav,
        name_eng: req.body.name_eng,
        price: req.body.price,
        class_nav: req.body.class_nav,
        class_eng: req.body.class_eng,
        ingredient_nav : req.body.ingredient_nav,
        ingredient_eng : req.body.ingredient_eng,
        desc_nav: req.body.desc_nav,
        desc_eng: req.body.desc_eng,
        country: req.body.country,
        mommys_id: req.body.mommys_id
    }}, function (err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
    //벨리데이트 쓸꺼면
    /*
     Tank.findById(id, function (err, tank) {
     if (err) return handleError(err);

     tank.size = 'large';
     tank.save(function (err) {
     if (err) return handleError(err);
     res.send(tank);
     });
     });
     */
};

exports.deleteDishes = function(req, res, next) {
    console.log("DELETE DISH");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //벨리데이트 안쓸꺼면 이방식
    DishesModel.findByIdAndDelete(new ObjectId(req.params.dishes_id.toString()), function (err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};



//나라별 디쉬와 나라별 마미스를 따로 보내는 방법,
//나라별 디쉬와, 마미스를 한json으로 만들어서 보내는 방법
exports.getDishesCountry = function(req, res, next) {
    console.log("GET Dishes Country");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));
    DishesModel.find({country:req.params.country}, function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};