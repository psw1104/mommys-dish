/**
 * Created by Woong on 2015. 7. 22..
 */

var mongoose = require('mongoose'),
    OrdersModel = mongoose.model('OrdersModel');

ObjectId = mongoose.Types.ObjectId;

/**
 * @api {post} /api/orders (앱)주문내역 생성(완료)
 * @apiName Create Order
 * @apiGroup Orders
 *
 * @apiParam {String} user_id 주문한 유져 Id
 * @apiParam {Array} dishes 주문한 음식, 및 수량 리스트 [{ "dish_id":"202020", "count":"2" },{ "dish_id":"23", "count":"3" }]
 * @apiParam {String} total_price 총 가격
 * @apiParam {String} name 수령자 이름
 * @apiParam {String} phone 수령자 전화번호
 * @apiParam {String} address 배달할 주소
 * @apiParam {String} order_time 주문시간
 * @apiParam {String} delivery_time 배달시간
 */
/**
 * @api {get} /api/orders/users/:user_id 유져의 주문내역 리스트 요청(완료)
 * @apiName Get Orderlist of user
 * @apiGroup Orders
 *
 */
/**
 * @api {get} /api/orders/:id 주문내역 정보 요청
 * @apiName Get Order
 * @apiGroup Orders
 *
 */
/**
 * @api {get} /api/orders/date/:date 해당 날짜의 주문내역
 * @apiName Get Order date
 * @apiGroup Orders
 *
 */
/**
 * @api {delete} /api/orders/:id 주문내역 삭제
 * @apiName delete Order
 * @apiGroup Orders
 *
 */


exports.createOrders = function(req, res, next) {

    console.log("CREATE ORDER");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //validate 부분은 나중에 추가
    var doc = new OrdersModel;

    doc.user_id = req.user_id;
    doc.dishes = req.body.dishes;
    doc.total_price = req.body.total_price;
    doc.name = req.body.name;
    doc.phone = req.body.phone;
    doc.address = req.body.address;
    doc.order_time = req.body.order_time;
    doc.delivery_time = req.body.delivery_time;

    doc.save(function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};



exports.getOrdersList = function(req, res, next) {
    console.log("GET ORDER");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    OrdersModel.find({user_id:req.params.user_id}, function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};