/**
 * Created by Woong on 2015. 7. 22..
 */
var mongoose = require('mongoose'),
    ReviewsModel = mongoose.model('ReviewsModel'),
    UsersModel = mongoose.model('UsersModel'),
    OrdersModel = mongoose.model('OrdersModel'),
    DishesModel = mongoose.model('DishesModel');

ObjectId = mongoose.Types.ObjectId;

/**
 * @api {post} /api/reviews 리뷰 생성
 * @apiName Create Review
 * @apiGroup Review
 *
 * @apiParam {Number} score 별점
 * @apiParam {String} comment 리뷰 내용
 * @apiParam {String} user_id 유져_id
 * @apiParam {String} user_email 이메일
 * @apiParam {String} dish_id dish_id
 * @apiParam {String} order_id order_id
 * @apiParam {Date} date 리뷰 날짜
 *
 */
/**
 * @api {put} /api/reviews/:id 리뷰 업데이트
 * @apiName Update Review
 * @apiParam {Number} score 별점
 * @apiParam {String} comment 리뷰 내용
 * @apiParam {Date} date 리뷰 날짜
 * @apiGroup Review
 */
/**
 * @api {delete} /api/reviews/:id 리뷰 삭제
 * @apiName Delete Review
 * @apiGroup Review
 */
/**
 * @api {get} /api/reviews/dishes/:dish_id/:skip 리뷰 리스트 요청
 * @apiName Get Reviewlist of dish
 * @apiGroup Orders
 */
/**
 * @api {get} /api/reviews/users/:user_id/:skip 유져의 리뷰보기 페이지
 * @apiName Get Reviewlist of user
 * @apiGroup Orders
 */
exports.createReviews = function(req, res, next) {

    console.log("CREATE REVIEWS");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //validate 부분은 나중에 추가
    var doc = new ReviewsModel;


    doc.score = req.body.score;
    doc.comment = req.body.comment;
    doc.user_id = req.body.user_id;
    doc.user_email = req.body.user_email;
    doc.dish_id = req.body.dish_id;
    doc.order_id = req.body.order_id;
    doc.date = req.body.date;


    doc.save(function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            //리뷰 평균 업데이트 작업 해야함.
            ReviewsModel.aggregate([
                {$match: {dish_id:data.dish_id}},
                {$group: {_id:data.dish_id ,average: {$avg: '$score'}}}
            ],function(err, average_data) {
                if(err) {
                    res.status(500);
                    res.json({code:500, message:err});
                } else {
                    //디쉬 평균 업데이트
                    DishesModel.findByIdAndUpdate(new ObjectId(data.dish_id.toString()), {avg_score: average_data[0].average},
                        function (err, data) {
                            if(err) {
                                res.status(500);
                                res.json({code:500, message:err});
                            } else {
                                res.status(200);
                                res.json({code:200, data:data});
                            }
                        });
                }
            });
        }
    });
};

exports.getReviewsDish = function(req, res, next) {
    console.log("GET REVIEWS DISH");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //리뷰 날짜순으로 정렬해서 10개 가져오기
    ReviewsModel.find({dish_id:req.params.dish_id},null,{skip:req.params.skip,limit:5,sort:{"date":-1}}, function(err,data){
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {

            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};


exports.deleteReviews = function(req, res, next) {
    console.log("DELETE REVIEWS");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //벨리데이트 안쓸꺼면 이방식
    ReviewsModel.findByIdAndRemove(new ObjectId(req.params.id.toString()), function (err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            ReviewsModel.aggregate([
                {$match: {dish_id:data.dish_id}},
                {$group: {_id:data.dish_id ,average: {$avg: '$score'}}}
            ],function(err, average_data) {
                if(err) {
                    res.status(500);
                    res.json({code:500, message:err});
                } else {
                    //디쉬 평균 업데이트
                    DishesModel.findByIdAndUpdate(new ObjectId(data.dish_id.toString()), {avg_score: average_data[0].average},
                        function (err, data) {
                            if(err) {
                                res.status(500);
                                res.json({code:500, message:err});
                            } else {
                                res.status(200);
                                res.json({code:200, data:data});
                            }
                        });
                }
            });
            //res.status(200);
            //res.json({code:200, data:data});
        }
    });
};

//완료
exports.updateReviews = function(req, res, next) {
    console.log("UPDATE REVIEWS");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //벨리데이트 안쓸꺼면 이방식
    ReviewsModel.findByIdAndUpdate(new ObjectId(req.params.id.toString()), {
        score: req.body.score,
        comment: req.body.comment,
        date: req.body.date
    }, function (err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            //res.status(200);
            //res.json({code:200, data:data});
            //디쉬 평균 업데이트
            ReviewsModel.aggregate([
                {$match: {dish_id:data.dish_id}},
                {$group: {_id:data.dish_id ,average: {$avg: '$score'}}}
            ],function(err, average_data) {
                if(err) {
                    res.status(500);
                    res.json({code:500, message:err});
                } else {
                    //업데이트 부분
                    DishesModel.findByIdAndUpdate(new ObjectId(data.dish_id.toString()), {avg_score: average_data[0].average},
                        function (err, data) {
                            if(err) {
                                res.status(500);
                                res.json({code:500, message:err});
                            } else {
                                res.status(200);
                                res.json({code:200, data:data});
                            }
                        });
                }
            });
        }
    });
};


exports.getReviewsUser = function(req, res, next) {
    console.log("GET REVIEWS User");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //먼저 주문내역을 5개 받아옴
    OrdersModel.find({user_id:req.params.user_id},null,{skip:req.params.skip,limit:5,sort:{"date":-1}}, function(err,order_data){
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(order_data));
            console.log("json count:"+order_data.length);
            var orderCount = order_data.length;
            while(order_data.length != 5) {
                order_data.push(JSON.parse('{"_id":null}'));
            }
            console.log(">>" + JSON.stringify(order_data));
            //5개를 읽어왔을때 배열의 남은갯수만큼 없는아이디값을 넣어줌
            //order_data.
            //5개의 오더데이터들 아이디를 오어로 해서 리뷰들을 긁어옴.

            ReviewsModel.find({order_id:{$in:[order_data[0]._id,order_data[1]._id,order_data[2]._id,order_data[3]._id,order_data[4]._id]}},
                function(err,review_data) {
                if(err) {
                    res.status(500);
                    res.json({code:500, message:err});
                } else {
                    console.log(">>" + JSON.stringify(review_data));
                    var res_data = [];
                    var index=0;
                    for(var i=0; i<orderCount; i++) {
                        console.log("i:"+i);
                        for(var j=0; j<order_data[i].dishes.length; j++) {
                            console.log("j:"+j);
                            var dish = JSON.parse("{}");
                            dish.dish_id = order_data[i].dishes[j].dish_id;
                            dish.order_id = order_data[i]._id;
                            dish.order_time = order_data[i].order_time;
                            review_data.forEach(function(review){
                                console.log("i:"+i+" j:"+j+JSON.stringify(review));
                                if(order_data[i].dishes[j].dish_id == review.dish_id) {
                                    console.log("dsfsdfsdfs");
                                    dish.review_id = review._id;
                                    dish.date = review.date;
                                    dish.user_email = review.user_email;
                                    dish.comment = review.comment;
                                    dish.score = review.score;
                                }
                            });

                            res_data.push(dish);
                            console.log("res_data: "+res_data);
                            //index++;

                        }
                    }
                    /*
                    for(var i=0; i<orderCount; i++) {
                        console.log("i:"+i);
                        for(var j=0; j<order_data[i].dishes.length; j++) {
                            console.log("j:"+j);
                            review_data.forEach(function(review){
                                console.log("i:"+i+" j:"+j+JSON.stringify(review));
                                if(order_data[i].dishes[j].dish_id == review.dish_id) {
                                    console.log("dsfsdfsdfs");
                                    order_data[i].dishes[j].review = review;
                                }
                            });
                        }
                    }*/
                    res.status(200);
                    res.json({code:200, data:res_data});
                }
            });
            //field: { $in: [<value1>, <value2>, ... <valueN> ] }

            //res.status(200);
            //res.json({code:200, data:order_data});
        }
    });


    //리뷰 날짜순으로 정렬해서 10개 가져오기
    /*
    ReviewsModel.find({dish_id:req.params.user_id},null,{skip:req.params.skip,limit:5,sort:{"date":-1}}, function(err,data){
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {

            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });*/
};
