/**
 * Created by Woong on 2015. 7. 22..
 */
var mongoose = require('mongoose'),
    MommysModel = mongoose.model('MommysModel');

ObjectId = mongoose.Types.ObjectId;

/**
 * @api {post} /api/mommys (운영)mommys 생성(완료)
 * @apiName Create Mommy
 * @apiGroup Mommys
 *
 * @apiParam {String} fisrtname_nav 본토이름
 * @apiParam {String} fisrtname_eng 영어이름
 * @apiParam {String} lastname_nav 본토이름
 * @apiParam {String} lastname_eng 영어이름
 * @apiParam {String} country 국적 (예: ko_KR)
 * @apiParam {String} style_nav 음식스타일(본토)
 * @apiParam {String} style_eng 음식스타일(영어)
 * @apiParam {String} intro_nav 설명(본토 어)
 * @apiParam {String} intro_eng 설명(영어)
 *
 *
 * @apiSuccess {String} 200
 *
 *
 */
/**
 * @api {get} /api/mommys (앱)마미스 리스트
 * @apiName Get Mommys List
 * @apiGroup Mommys
 *
 *
 */
/**
 * @api {get} /api/mommys/country/:country (앱)해당 counrty mommys 리스트 요청(완료)
 * @apiName Get Country Mommys List
 * @apiGroup Mommys
 *
 *
 */
/**
 * @api {get} /api/mommys/:id (앱)mommys 정보 요청(완료)
 * @apiName Get Mommy
 * @apiGroup Mommys
 *
 *
 * @apiParam {String} fisrtname_nav 본토이름
 * @apiParam {String} fisrtname_eng 영어이름
 * @apiParam {String} lastname_nav 본토이름
 * @apiParam {String} lastname_eng 영어이름
 * @apiParam {String} country 국적 (예: ko_KR)
 * @apiParam {String} style_nav 음식스타일(본토)
 * @apiParam {String} style_eng 음식스타일(영어)
 * @apiParam {String} intro_nav 설명(본토 어)
 * @apiParam {String} intro_eng 설명(영어)
 */
/**
 * @api {put} /api/mommys/:id (운영)mommys 수정(완료)
 * @apiName Update Mommy
 * @apiGroup Mommys
 *
 * @apiParam {String} fisrtname_nav 본토이름
 * @apiParam {String} fisrtname_eng 영어이름
 * @apiParam {String} lastname_nav 본토이름
 * @apiParam {String} lastname_eng 영어이름
 * @apiParam {String} country 국적 (예: ko_KR)
 * @apiParam {String} style_nav 음식스타일(본토)
 * @apiParam {String} style_eng 음식스타일(영어)
 * @apiParam {String} intro_nav 설명(본토 어)
 * @apiParam {String} intro_eng 설명(영어)
 *
 *
 */
/**
 * @api {delete} /api/mommys/:id (운영)mommys 삭제(완료)
 * @apiName Delete Mommy
 * @apiGroup Mommys
 *
 */
exports.createMommys = function(req, res, next) {

    console.log("CREATE MOMMY");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //validate 부분은 나중에 추가
    //MommysModel.schema.path('country').validate(function (value) {
    //    return /korea|china|Ko-Kr/i.test(value);
    //}, 'Invalid country');
    var doc = new MommysModel;
    doc.firstname_nav = req.body.firstname_nav;
    doc.firstname_eng = req.body.firstname_eng;
    doc.lastname_nav = req.body.lastname_nav;
    doc.lastname_eng = req.body.lastname_eng;
    doc.country = req.body.country;
    doc.style_nav = req.body.style_nav;
    doc.style_eng = req.body.style_eng;
    doc.intro_nav = req.body.intro_nav;
    doc.intro_eng = req.body.intro_eng;

    doc.save(function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};
exports.getMommysList = function(req, res, next) {
    console.log("GET total Mommys List");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    MommysModel.find(true, function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};
exports.getMommysCountry = function(req, res, next) {
    console.log("GET Mommys Country");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    MommysModel.find({country:req.params.country}, function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};


exports.getMommys = function(req, res, next) {
    console.log("GET MOMMY");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    MommysModel.findOne({_id:new ObjectId(req.params.mommys_id.toString())}, function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};

exports.updateMommys = function(req, res, next) {
    console.log("UPDATE MOMMY");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //벨리데이트 안쓸꺼면 이방식
    MommysModel.findByIdAndUpdate(new ObjectId(req.params.id.toString()), { $set: {
        intro_eng: req.body.intro_eng,
        intro_nav: req.body.intro_nav,
        country: req.body.country,
        style_nav : req.body.style_nav,
        style_eng : req.body.style_eng,
        firstname_nav : req.body.firstname_nav,
        firstname_eng : req.body.firstname_eng,
        lastname_nav : req.body.lastname_nav,
        lastname_eng : req.body.lastname_eng
    }}, function (err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
    //벨리데이트 쓸꺼면
    /*
    Tank.findById(id, function (err, tank) {
        if (err) return handleError(err);

        tank.size = 'large';
        tank.save(function (err) {
            if (err) return handleError(err);
            res.send(tank);
        });
    });
    */
};
exports.deleteMommys = function(req, res, next) {
    console.log("DELETE MOMMY");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    //벨리데이트 안쓸꺼면 이방식
    MommysModel.findByIdAndRemove(new ObjectId(req.params.id.toString()), function (err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};
