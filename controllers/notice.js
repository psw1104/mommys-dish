/**
 * Created by Woong on 2015. 8. 17..
 */
var mongoose = require('mongoose'),
    NoticeModel = mongoose.model('NoticeModel');

ObjectId = mongoose.Types.ObjectId;
/**
 * @api {post} /api/notices (운영)notice 생성(완료)
 * @apiName Create notice
 * @apiGroup Notice
 *
 * @apiParam {String} title 제목
 * @apiParam {String} content 내용
 * @apiParam {Date} date 날짜
 *
 */
/**
 * @api {get} /api/notices (앱)노티스 리스트
 * @apiName Get Notice List
 * @apiGroup Notice
 *
 *
 */
exports.createNotice = function(req, res, next) {

    console.log("CREATE NOTICE");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));


    var doc = new NoticeModel;
    doc.title = req.body.title;
    doc.date = req.body.date;
    doc.content = req.body.content;

    doc.save(function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};
exports.getNoticeList = function(req, res, next) {
    console.log("GET total NOTICE List");
    console.log("params =>" + JSON.stringify(req.params));
    console.log("body =>" + JSON.stringify(req.body));

    NoticeModel.find(true, function(err, data) {
        if(err) {
            res.status(500);
            res.json({code:500, message:err});
        } else {
            console.log(">>" + JSON.stringify(data));
            res.status(200);
            res.json({code:200, data:data});
        }
    });
};