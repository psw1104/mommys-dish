/**
 * Created by Woong on 2015. 7. 22..
 */
var restify = require('restify')
    , fs = require('fs')
    , mongoose = require('mongoose');

var controllers = {}
    , controllers_path = process.cwd() + '/controllers';

fs.readdirSync(controllers_path)
    .forEach(function (file) {

   if(file.indexOf('.js') != -1) {
       controllers[file.split('.')[0]] = require(controllers_path + '/' + file);
   }
});

var server = restify.createServer();

var firstFunc = function(req, res, next) {
    console.log("111111111111");
    console.log("req firstStr>" + req.firstStr);
    console.log("req secondStr>" + req.secondStr);
    req.firstStr = "!!!";


    next();
} ;

var secondFunc = function(req, res, next) {
    console.log("22222222222");
    console.log("req firstStr>" + req.firstStr);
    console.log("req secondStr>" + req.secondStr);
    req.secondStr = "???";
    next();
} ;


var setup_server = function(app) {
    app.use(restify.fullResponse())
        .use(restify.bodyParser())
        .use(function(req, res, next) {
           next();
        });

    app.get("/api/test", firstFunc, secondFunc, controllers.test.test_function);

    app.post("/api/mommys", controllers.mommys.createMommys);
    app.put("/api/mommys/:id", controllers.mommys.updateMommys);
    app.del("/api/mommys/:id", controllers.mommys.deleteMommys);
    app.get("/api/mommys", controllers.mommys.getMommysList);
    app.get("/api/mommys/:id", controllers.mommys.getMommys);
    app.get("/api/mommys/country/:country", controllers.mommys.getMommysCountry);


    app.post("/api/dishes", controllers.dishes.createDishes);
    app.put("/api/dishes/:id", controllers.dishes.updateDishes);
    app.del("/api/dishes/:id", controllers.dishes.deleteDishes);
    app.get("/api/dishes/country/:country", controllers.dishes.getDishesCountry);
    app.get("/api/dishes", controllers.dishes.getDishesList);
    app.get("/api/dishes/:id", controllers.dishes.getDishes);



    app.post("/api/users", controllers.users.createUser);
    app.put("/api/users/:id", controllers.users.updateUser);
    app.post("/api/login", controllers.users.login);


    app.post("/api/orders", controllers.users.checkSession, controllers.orders.createOrders);
    app.get("/api/orders/users/:user_id", controllers.orders.getOrdersList);

    app.post("/api/reviews", controllers.reviews.createReviews);
    app.put("/api/reviews/:id", controllers.reviews.updateReviews);
    app.del("/api/reviews/:id", controllers.reviews.deleteReviews);
    app.get("/api/reviews/users/:user_id/:skip", controllers.reviews.getReviewsUser);
    app.get("/api/reviews/dishes/:dish_id/:skip", controllers.reviews.getReviewsDish);
    //app.get("/api/mommys/:mommys_id", controllers.mommys.getMommys);


    app.get("/api/notices", controllers.notice.getNoticeList);
    app.post("/api/notices", controllers.notice.createNotice);

};

setup_server(server);

server.listen(8080, function() {
   console.log("%s listening at %s", server.name, server.url);
});
