/**
 * Created by Woong on 2015. 7. 22..
 */
var mongoose = require('mongoose')
    , fs = require('fs')
    , models_path = process.cwd() + '/models';

var dsn = "mongodb://localhost:27017/mommysdish";
var connect_option = { server: {auto_reconnect: true}};
mongoose.connect(dsn, connect_option);

var db = mongoose.connection;

db.on ('error', function(err) {
    console.error ("Mongodb connection error:", err);
});

db.once('open', function callback() {
    console.info("Mongodb connection is established");
});

db.on('disconnected', function() {
    console.error('Mongodb disconnected');
    mongoose.connect(dsn, connect_option);
});

db.on('reconnected', function() {
    console.info("mongodb reconnected");
});

fs.readdirSync(models_path).forEach(function(file) {
    //console.log("model>>>" + file);
    if(~file.indexOf('.js')) {
        require(models_path + '/' + file);
    }
});