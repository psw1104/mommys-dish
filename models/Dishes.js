/**
 * Created by Woong on 2015. 7. 22..
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var DishesSchema = new Schema ({
    "name_nav" : String,
    "name_eng" : String,
    "price" : String,
    "class_nav" : String,
    "class_eng" : String,
    "ingredient_nav" : String,
    "ingredient_eng" : String,
    "desc_nav" : String,
    "desc_eng" : String,
    "country" : String, // "ko_kr "
    "mommys_id" : String,
    "avg_score" : Number
}, {versionKey: false});

mongoose.model('DishesModel', DishesSchema);