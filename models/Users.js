/**
 * Created by Woong on 2015. 7. 22..
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UsersSchema = new Schema ({
    "user_key" : String,
    "email" : String,       //필수로 받기
    "password" : String,    //해쉬로 변환한 값
    "type" : String, //F,G,N facebook,google,normal
    "name" : String,  //주문 시 필요한 정보들
    "phone" : String,
    "address" : String,
    "birthdate" : Date,
    "residential" : String
}, {versionKey: false});

mongoose.model('UsersModel', UsersSchema);