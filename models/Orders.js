/**
 * Created by Woong on 2015. 7. 22..
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OrdersSchema = new Schema ({
    "user_id" : String,     //user._id
    "dishes" : Array,       //주문 내역 페이지에서 모든음식에대한 정보를 가지고있지 않을시 어떻게 보여줘야하나.
    "total_price" : String,     //최종 결제 금액
    "name" : String,
    "phone" : String,
    "address" : String,
    "order_time" : Date,
    "delivery_time" : Date
}, {versionKey: false});

mongoose.model('OrdersModel', OrdersSchema);