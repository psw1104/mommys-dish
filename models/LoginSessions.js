/**
 * Created by Woong on 2015. 7. 24..
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SessionsSchema = new Schema ({
    "token" : String,    ///인덱스를 업데이트 데이트 에 걸면서 익스파이어드 타임을 정할수 있다.  로그인 정보가 필요한 api들은 엑세스토큰을 파라미터로 물고감
    "user_id" : String
}, {versionKey: false});

mongoose.model('SessionsModel', SessionsSchema);