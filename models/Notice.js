/**
 * Created by Woong on 2015. 8. 17..
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var NoticeSchema = new Schema ({
    "title" : String,
    "content": String,
    "date" : Date,
    // Number, Array, Object
}, {versionKey: false});

mongoose.model('NoticeModel', NoticeSchema);