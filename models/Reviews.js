/**
 * Created by Woong on 2015. 7. 22..
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReviewsSchema = new Schema ({
    "score" : Number,
    "comment" : String,
    "user_id" : String,     //key값
    "user_email" : String,
    //"user_email" : String,  매번 검색해서 보여줘야할지.   쿼리를 한번에 날려서 유져정보를 가져오고 하는식으로. 그리고 조인해서
    "dish_id" : String,
    "order_id" : String,
    "date" : Date
}, {versionKey: false});

mongoose.model('ReviewsModel', ReviewsSchema);