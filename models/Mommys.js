/**
 * Created by Woong on 2015. 7. 22..
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MommysSchema = new Schema ({
    "firstname_nav" : String,
    "firstname_eng" : String,
    "lastname_nav" : String,
    "lastname_eng" : String,
    "country" : String, // "ko_kr "
    "style_nav" : String,
    "style_eng" : String,
    "intro_nav" : String,
    "intro_eng" : String
    // Number, Array, Object
}, {versionKey: false});

mongoose.model('MommysModel', MommysSchema);